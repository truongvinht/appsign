/*
 
 AppDelegate.m
 AppSign
 
 Copyright (c) 25/09/2014 Truong Vinh Tran
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#import "AppDelegate.h"

//import to get provisioning profiles
#import "VProvisioningManager.h"

///tags for the button action
typedef enum{
    ActionButtonTagIPA = 21,
    ActionButtonTagEntitlement = 23,
    ActionButtonTagConfirm = 24
}ActionButtonTag;

///different level for the indicator
typedef enum{
    ProcessStatusLevelBegin = 10,
    ProcessStatusLevelUnpack = 20,
    ProcessStatusLevelSignApp = 30,
    ProcessStatusLevelVerify = 40,
    ProcessStatusLevelPack = 50,
    ProcessStatusLevelRemoveTmp= 60
}ProcessStatusLevel;


@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;

/// list with all available certificates for signing and profile list for signing
@property(nonatomic,strong) NSArray *certificateList,*profileList;

/// task for handling getting certificates
@property(nonatomic,strong) NSTask *certTask;

/// object to handle the sign process
@property(nonatomic,strong) VSignManager *signManager;

@end

@implementation AppDelegate

- (void)setViewEnabled:(BOOL)enabled{
    [_ipaTextfield setEnabled:enabled];
    [_ipaButton setEnabled:enabled];
    [_entitlementTextfield setEnabled:enabled];
    [_entitlementButton setEnabled:enabled];
    [_certificateCombobox setEnabled:enabled];
    [_confirmButton setEnabled:enabled];
    [_appIDButton setEnabled:enabled];
    if (enabled) {
        [_appIDTextField setEnabled:_appIDButton.state==NSOnState];
        [_provisionCombobox setEnabled:[_certificateCombobox.stringValue length]>0];
    }else{
        [_appIDTextField setEnabled:NO];
        [_provisionCombobox setEnabled:NO];
    }
    
}

/** Method to load placeholder text for all textfields*/
- (void)loadPlaceHolderText{
    
    //ipa text field
    NSTextFieldCell *ipaCell = [self.ipaTextfield cell];
    [ipaCell setPlaceholderString:@"/path/to/app.ipa"];
    [_ipaTextfield setFileType:@"ipa"];
    
    //entitlement text field
    NSTextFieldCell *entitlementCell = [self.entitlementTextfield cell];
    [entitlementCell setPlaceholderString:@"/path/to/entitlements.plist"];
    [_entitlementTextfield setFileType:@"plist"];
    
    //app bundle text field
    NSTextFieldCell *bundleCell = [self.appIDTextField cell];
    [bundleCell setPlaceholderString:@"com.companyname.appname"];
    
    //app bundle text field
    NSComboBoxCell *signCell = [self.certificateCombobox cell];
    [signCell setPlaceholderString:@"Select a signing certificate"];
    
    //provisioning profile text field
    NSComboBoxCell *provisioningCell = [self.provisionCombobox cell];
    [provisioningCell setPlaceholderString:@"Select a provisioning profile"];
    
}

- (void)loadButtonText{
    
    NSString *browsingTitle = NSLocalizedString(@"Browse", @"Text for browsing file");
    [self.ipaButton setTitle:browsingTitle];
    [self.entitlementButton setTitle:browsingTitle];
    
    NSString *updateAppID = NSLocalizedString(@"Change ID", @"Change Application Bundle ID");
    [self.appIDButton setTitle:updateAppID];
    
    NSString *signing = NSLocalizedString(@"Signing App", @"Signing Application");
    [self.confirmButton setTitle:signing];
}

- (void)updateStatusMessage:(NSString*)message{
    
    //status label
    NSTextFieldCell *statusCell = [self.statusLabel cell];
    [statusCell setTitle:message];
}

- (void)initButtons{
    
    //ipa browse button
    NSButtonCell *ipaButtonCell= [_ipaButton cell];
    [ipaButtonCell setTarget:self];
    [ipaButtonCell setAction:@selector(buttonPressed:)];
    
    //entitlement button
    NSButtonCell *entitleButtonCell = [_entitlementButton cell];
    [entitleButtonCell setTarget:self];
    [entitleButtonCell setAction:@selector(buttonPressed:)];
    
    //confirm button
    NSButtonCell *confirmButtonCell = [_confirmButton cell];
    [confirmButtonCell setTarget:self];
    [confirmButtonCell setAction:@selector(buttonPressed:)];
    
    //app id change
    NSButtonCell *allowAppIDChangeCell = [_appIDButton cell];
    [allowAppIDChangeCell setTarget:self];
    [allowAppIDChangeCell setAction:@selector(toggleAppID:)];
    
    //certificate changed
    NSComboBoxCell *certificateCell = [_certificateCombobox cell];
    [certificateCell setTarget:self];
    [certificateCell setAction:@selector(updateProvisioning:)];
}


#pragma mark - Button Action Methods

- (void)toggleAppID:(NSButton*)sender{
    
    [_appIDTextField setEnabled:sender.state == NSOnState];
    
    //make the textfield active if enabled
    if (_appIDTextField.isEnabled) {
        [_appIDTextField becomeFirstResponder];
    }
    
}
- (void)browseFileType:(NSArray*)types initDir:(NSString*)dir completion:(void (^)(NSString *path))completion{
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    
    [openDlg setCanChooseFiles:YES];
    [openDlg setCanChooseDirectories:NO];
    [openDlg setAllowsMultipleSelection:NO];
    [openDlg setAllowsOtherFileTypes:NO];
    [openDlg setAllowedFileTypes:types];
    
    if (dir) {
        [openDlg setDirectoryURL:[NSURL fileURLWithPath:dir]];
    }
    
    if ([openDlg runModal] == NSOKButton)
    {
        if (completion) {
            NSString* fileNameOpened = [[[openDlg URLs] objectAtIndex:0] path];
            completion(fileNameOpened);
        }
    }
}

/** Method to handle button actions
 *  @param sender is the button which was pressed
 */
- (void)buttonPressed:(NSButton*)sender{
    
    //check which button pressed
    switch ((ActionButtonTag)[(NSButtonCell*)[sender cell] tag]) {
        case ActionButtonTagIPA:{
            //show the browse for IPA
            [self browseFileType:@[@"ipa",@"IPA"] initDir:nil completion:^(NSString *path){
                [_ipaTextfield setStringValue:path];
            }];
        }break;;
        case ActionButtonTagEntitlement:{
            //show the browse for the entitlement
            [self browseFileType:@[@"plist",@"PLIST"]initDir:nil completion:^(NSString *path){
                [_entitlementTextfield setStringValue:path];
            }];
        }break;
        case ActionButtonTagConfirm:{
            // start signing application
            //disable interaction
            [self setViewEnabled:NO];
            [_progressbar setIndeterminate:NO];
            [_progressbar setHidden:NO];
            [_progressbar setDoubleValue:ProcessStatusLevelBegin];
            
            self.signManager = [[VSignManager alloc] initWithTarget:self];
            
            if([_certificateCombobox indexOfSelectedItem]>=0){
                self.signManager.certificate = [_certificateList objectAtIndex:[_certificateCombobox indexOfSelectedItem]];
            }
            
            //read provisioining profile
            NSString *provisionProfile = _provisionCombobox.stringValue;
            NSString *provisionPath = nil;
            if ([provisionProfile length]>0) {
                NSString *index = [[provisionProfile componentsSeparatedByString:@":"] firstObject];
                int pos = [index intValue]-1;
                
                if (pos < _profileList.count) {
                    provisionPath = [[_profileList objectAtIndex:pos] objectForKey:@"provisioning-path"];
                    //NSLog(@"PROV %@",provisionPath);
                }
            }
            
            [self.signManager startSign:[_ipaTextfield.stringValue copy] usingProvision:provisionPath usingEntitlement:[_entitlementTextfield.stringValue copy]];
        }break;
            
        default:
            break;
    }
}

- (void)updateProvisioning:(NSComboBox*)combobox{
    [VProvisioningManager sharedInstance].tmpFolder = [VSignManager workingDirectory];
    
    self.profileList = [[[VProvisioningManager sharedInstance] validProfilesFor:combobox.stringValue] sortedArrayUsingComparator:^(NSMutableDictionary *obj1,NSMutableDictionary *obj2) {
        NSString *num1 =[obj1 objectForKey:@"Name"];
        NSString *num2 =[obj2 objectForKey:@"Name"];
        return (NSComparisonResult) [num1 compare:num2 options:(NSLiteralSearch)];
    }];
    
    [self.provisionCombobox reloadData];
    [self.provisionCombobox setEnabled:YES];
}

#pragma mark - Load Certificates for Signing

- (void)loadCertificates{
    self.certificateList = @[];
    
    //display status
    [self.statusLabel setStringValue:NSLocalizedString(@"Getting Signing Certificate IDs", @"Status Message for Loading Certificates")];
    
    //create thread to parse available certificates
    self.certTask = [[NSTask alloc] init];
    [_certTask setLaunchPath:@"/usr/bin/security"];
    [_certTask setArguments:@[@"find-identity",@"-v",@"-p",@"codesigning"]];
    
    //regular check for certificates (every second)
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkForCertificates:) userInfo:nil repeats:YES];
    
    //monitor actions with one pipe
    NSPipe *pipe = [NSPipe pipe];
    [_certTask setStandardOutput:pipe];
    [_certTask setStandardError:pipe];
    
    NSFileHandle *handle = [pipe fileHandleForReading];
    
    //start task
    [_certTask launch];
    
    [NSThread detachNewThreadSelector:@selector(watchReadingCertificates:) toTarget:self withObject:handle];
}

- (void)watchReadingCertificates:(NSFileHandle*)streamHandle {
    @autoreleasepool {
        
        NSString *securityResult = [[NSString alloc] initWithData:[streamHandle readDataToEndOfFile] encoding:NSUTF8StringEncoding];
        
        // Verify the security result
        if (securityResult == nil || securityResult.length < 1) {
            // Nothing in the result, return
            return;
        }
        NSArray *rawResult = [securityResult componentsSeparatedByString:@"\""];
        NSMutableArray *tempGetCertsResult = [NSMutableArray arrayWithCapacity:20];
        for (int i = 0; i <= [rawResult count] - 2; i+=2) {
            
            if (rawResult.count - 1 < i + 1) {
                // Invalid array, don't add an object to that position
            } else {
                // Valid object
                [tempGetCertsResult addObject:[rawResult objectAtIndex:i+1]];
            }
        }
        
        self.certificateList = [[NSArray arrayWithArray:tempGetCertsResult] sortedArrayUsingSelector:@selector(compare:)];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_certificateCombobox reloadData];
        });
        
        
    }
}

- (void)checkForCertificates:(NSTimer*)timer{
    if ([_certTask isRunning] == 0) {
        [timer invalidate];
        _certTask = nil;
        
        if ([_certificateList count] > 0) {
            [self setViewEnabled:YES];
            //[_statusLabel setStringValue:@"Signing Certificate IDs extracted"];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_statusLabel setStringValue:@""];
                [_certificateCombobox setStringValue:@""];
            });
//            if ([defaults valueForKey:@"CERT_INDEX"]) {
//                
//                NSInteger selectedIndex = [[defaults valueForKey:@"CERT_INDEX"] integerValue];
//                if (selectedIndex != -1) {
//                    NSString *selectedItem = [self comboBox:certComboBox objectValueForItemAtIndex:selectedIndex];
//                    [certComboBox setObjectValue:selectedItem];
//                    [certComboBox selectItemAtIndex:selectedIndex];
//                }
//                
//                //[self enableControls];
//            }
        } else {
            //[self showAlertOfKind:NSCriticalAlertStyle WithTitle:@"Error" AndMessage:@"Getting Certificate ID's failed"];
            //[self enableControls];
            [_statusLabel setStringValue:@"Ready"];
        }
    }
}

#pragma mark - Combobox Delegate

-(NSInteger)numberOfItemsInComboBox:(NSComboBox *)comboBox {
    NSInteger count = 0;
    if ([comboBox isEqual:_certificateCombobox]) {
        count = [_certificateList count];
    }
    
    if ([comboBox isEqual:_provisionCombobox]) {
        count = [_profileList count];
    }
    
    return count;
}



- (id)comboBox:(NSComboBox *)comboBox objectValueForItemAtIndex:(NSInteger)index {
    id item = nil;
    if ([comboBox isEqual:_certificateCombobox]) {
        item = [_certificateList objectAtIndex:index];
    }
    
    if ([comboBox isEqual:_provisionCombobox]) {
        NSDictionary *profile = [_profileList objectAtIndex:index];
        NSString *profileName = [profile objectForKey:@"Name"];
        
        NSString *teamID = [[profile objectForKey:@"Entitlements"] objectForKey:@"application-identifier"];
        
        if (!teamID) {
            teamID = @"";
        }else{
            teamID = [NSString stringWithFormat:@"(%@)",teamID];
        }
        item = [NSString stringWithFormat:@"%li: %@ %@",index+1,profileName,teamID];
    }
    
    return item;
}

#pragma mark - VSignManager Methods

- (void)updateSignStatus:(NSString *)statusMessage{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_progressbar setDoubleValue:_progressbar.doubleValue+10];
        [_statusLabel setStringValue:statusMessage];
    });
}

- (void)finishedSignApplication:(NSString *)report{
    
    //show error message
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.progressbar setHidden:YES];
        [self.progressbar setDoubleValue:0];
        [self setViewEnabled:YES];
        [self.statusLabel setStringValue:@""];
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"Ok Button")];
        [alert setMessageText:NSLocalizedString(@"Information", @"Information title")];
        [alert setInformativeText:report];
        [alert setAlertStyle:NSInformationalAlertStyle];
        [alert runModal];
    });
}

- (void)failSignApplication:(NSString*)errorMessage{
    [self.progressbar setHidden:YES];
    [self.progressbar setDoubleValue:0];
    [self setViewEnabled:YES];
    
    //show error message
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.statusLabel setStringValue:@""];
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:NSLocalizedString(@"OK", @"Ok Button")];
        [alert setMessageText:NSLocalizedString(@"Error", @"Error title")];
        [alert setInformativeText:errorMessage];
        [alert setAlertStyle:NSCriticalAlertStyle];
        [alert runModal];
    });
}

#pragma mark - Application Methods

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    self.profileList = [NSArray new];
    
    //init labels
    [self loadPlaceHolderText];
    [self loadButtonText];
    [self updateStatusMessage:@""];
    [self.progressbar setHidden:YES];
    
    NSRect frame = self.certificateCombobox.frame;
    
    frame.size.height = 22;
    self.certificateCombobox.frame =frame;
    
    //init button actions
    [self initButtons];
    
    //start a new thread for parsing certificates
    [self loadCertificates];
    
    [self setViewEnabled:NO];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
    
    //clean up
    if (self.signManager) {
        [self.signManager abort];
    }
}

//Called when clicked close option on window
- (BOOL)applicationShouldTerminateAfterLastWindowClosed: (NSApplication *) theApplication
{
    [[NSApplication sharedApplication] hide:self];
    return NO;
}

//Called when you tap app icon on dock.
-(BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag{
    [[self window] makeKeyAndOrderFront:self];
    return YES;
}

@end
