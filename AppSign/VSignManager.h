/*
 
 VSignManager.h
 AppSign
 
 Copyright (c) 26/09/2014 Truong Vinh Tran
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#import <Foundation/Foundation.h>


@protocol VSignManagerDelegate <NSObject>

@optional
/** Method will be called to pass more information about the status
 *  @param statusMessage is the current status message
 */
- (void)updateSignStatus:(NSString*)statusMessage;


/** Method will be called if the sign process failed
 *  @param errorMessage is the message about the error
 */
- (void)failSignApplication:(NSString*)errorMessage;
 
@required
/** Method will be called after finish sign application
 *  @param report is the signature data
 */
- (void)finishedSignApplication:(NSString*)report;

@end

@interface VSignManager : NSObject

/// target class to handle results
@property(nonatomic,weak) id<VSignManagerDelegate> delegate;

/// new bundle name for the app
@property(nonatomic,strong) NSString *bundleName;

/// new bundle version number (e.g. 1.0.1)
@property(nonatomic,strong) NSString *bundleVersionLong;

/// name of the certificate for signing app
@property(nonatomic,strong) NSString *certificate;

/** Method to init a new manager instance
 *  @param target is the target which receive the actions
 *  @return new instance of VSignManager
 */
- (id)initWithTarget:(id<VSignManagerDelegate>)target;

/** Method to start sign process
 *  @param ipa is the path to the ipa file
 *  @param provision is the path to the mobile provisioning
 *  @param entitlement is the path to the entitlement
 */
- (void)startSign:(NSString*)ipa usingProvision:(NSString*)provision usingEntitlement:(NSString*)entitlement;

/** Method to stop and cancel running sign process*/
- (void)abort;

/** Method to get the working directory
 *  @return get the working directory
 */
+ (NSString*)workingDirectory;

@end
