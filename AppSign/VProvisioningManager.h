/*
 
 VProvisioningManager.h
 AppSign
 
 Copyright (c) 27/09/2014 Truong Vinh Tran
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#import <Foundation/Foundation.h>

/*! Class to handle Provisioning Profiles*/
@interface VProvisioningManager : NSObject

/// folder for the tmp
@property(nonatomic,strong) NSString *tmpFolder;

/** Singleton instance to access provisioning profiles
 *  @return new instance
 */
+ (VProvisioningManager*)sharedInstance;

/** Method to get the list of available profiles
 *  @param codeSignID is the Code Signing Identity
 *  @return a list with all valid profiles (NSDictionary)
 */
- (NSArray*)validProfilesFor:(NSString*)codeSignID;

@end
