//
//  main.m
//  AppSign
//
//  Created by Vinh Tran on 25/09/14.
//  Copyright (c) 2014 Vinh Tran. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
