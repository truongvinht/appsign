/*
 
 AppDelegate.h
 AppSign
 
 Copyright (c) 25/09/2014 Truong Vinh Tran
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#import <Cocoa/Cocoa.h>

//import to support drag/drop items to text field
#import "VPathTextField.h"

//import to sign application
#import "VSignManager.h"


@interface AppDelegate : NSObject <NSApplicationDelegate,VSignManagerDelegate>

/// textfield for inserting the path to the IPA
@property (weak) IBOutlet VPathTextField *ipaTextfield;

/// textfield for the path of the entitlement file
@property (weak) IBOutlet VPathTextField *entitlementTextfield;

/// textfield for the new application id
@property (weak) IBOutlet VPathTextField *appIDTextField;

/// button for browsing for the IPA
@property (weak) IBOutlet NSButton *ipaButton;

/// button for broswing the entitlement
@property (weak) IBOutlet NSButton *entitlementButton;

/// button for enable/disable app bundle editing
@property (weak) IBOutlet NSButton *appIDButton;

/// button to start resign application
@property (weak) IBOutlet NSButton *confirmButton;

/// list with available certificates for signing application
@property (weak) IBOutlet NSComboBox *certificateCombobox;

/// combo box for the provisioning profile
@property (weak) IBOutlet NSComboBox *provisionCombobox;

/// indicator for processing
@property (weak) IBOutlet NSProgressIndicator *progressbar;

/// label for the status
@property (weak) IBOutlet NSTextField *statusLabel;
@end

