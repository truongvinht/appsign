/*
 
 VSignManager.m
 AppSign
 
 Copyright (c) 26/09/2014 Truong Vinh Tran
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#import "VSignManager.h"


#define WORKING_DIR [NSTemporaryDirectory() stringByAppendingPathComponent:@"CCWTMP"];
#define VPAYLOAD @"Payload"
#define VINFO_PLIST @"Info.plist"
#define VAPP @"app"
#define VIPA @"ipa"

//keys for the plist
#define V_BUNDLE_ID_CHANGE @"keyBundleIDChange"
#define V_BUNDLE_ID_PLIST_APP @"CFBundleIdentifier"
#define V_BUNDLE_ID_PLIST_ITUNES_ARTWORK @"softwareVersionBundleId"


//suffix for the signed app
#define VADDITIONAL_SUFFIX @"-resigned"

@interface VSignManager ()

/// path to the IPA, provision,entitlement
@property(nonatomic,strong) NSString *pathToIPA,*pathToProvision,*pathToEntitlement;

/// information about the codesign
@property(nonatomic,strong) NSString *codesignData, *verificationData;

/// working tmp path
@property(nonatomic,strong) NSString *workingDir,*applicationDir;

/// task for unzip
@property(nonatomic,strong) NSTask *unzipTask,*provisionTask,*codesignTask,*verifyTask, *zipTask;

@end

@implementation VSignManager

- (id)initWithTarget:(id<VSignManagerDelegate>)target{
    self = [super init];
    
    if (self) {
        self.delegate = target;
    }
    
    return self;
}

#pragma mark - Preparing

- (void)failAndClean:(NSString*)message{
    if ([_delegate respondsToSelector:@selector(failSignApplication:)]) {
        [_delegate failSignApplication:message];
    }
    
    // clean up
    [self cleanTMP];
}

- (void)cleanTMP{
    
    NSError *removeError = nil;
    
    //remove old tmp folder
    if([[NSFileManager defaultManager] fileExistsAtPath:_workingDir]){
        if(![[NSFileManager defaultManager] removeItemAtPath:self.workingDir error:&removeError]){
#ifdef DEBUG
            NSLog(@"VSignManager#%@:%@",NSStringFromSelector(_cmd),[removeError description]);
#endif
        }
    }
}

- (void)prepareWorkingDirectory{
    
    //assign working dir
    self.workingDir = WORKING_DIR;
    
    //remove old tmp folder
    [self cleanTMP];
    
    //create new tmp folder
    [[NSFileManager defaultManager] createDirectoryAtPath:_workingDir withIntermediateDirectories:YES attributes:nil error:nil];
}

#pragma mark - Unzip

- (void)unzip{
    self.unzipTask = [NSTask new];
    [_unzipTask setLaunchPath:@"/usr/bin/unzip"];
    [_unzipTask setArguments:@[@"-q",_pathToIPA,@"-d",_workingDir]];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkUnzip:) userInfo:nil repeats:YES];
    [_unzipTask launch];
}

- (void)checkUnzip:(NSTimer *)timer {
    if ([_unzipTask isRunning] == 0) {
        [timer invalidate];
        _unzipTask = nil;
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[_workingDir stringByAppendingPathComponent:@"Payload"]]) {
            
            if ([_delegate respondsToSelector:@selector(updateSignStatus:)]) {
                [_delegate updateSignStatus:NSLocalizedString(@"Original app extracted", @"Extract app")];
            }
            
            //check wether bundle name needs to be updated
            if (_bundleName) {
                [self doBundleIDChange:_bundleName];
            }
            
            if (_pathToProvision) {
                [self doProvisioning];
            }else{
                //start sign application
                [self signApplication];
            }
            
        } else {
            //clean up
            [self failAndClean:NSLocalizedString(@"Unzip failed", @"Unzip failed")];
        }
    }
}

#pragma mark - Bundle Update

- (BOOL)doBundleIDChange:(NSString *)newBundleID {
    BOOL success = YES;
    
    success &= [self doAppBundleIDChange:newBundleID];
    success &= [self doITunesMetadataBundleIDChange:newBundleID];
    
    return success;
}


- (BOOL)doITunesMetadataBundleIDChange:(NSString *)newBundleID {
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:_workingDir error:nil];
    NSString *infoPlistPath = nil;
    
    for (NSString *file in dirContents) {
        if ([[[file pathExtension] lowercaseString] isEqualToString:@"plist"]) {
            infoPlistPath = [_workingDir stringByAppendingPathComponent:file];
            break;
        }
    }
    
    return [self changeBundleIDForFile:infoPlistPath bundleIDKey:V_BUNDLE_ID_PLIST_ITUNES_ARTWORK newBundleID:newBundleID plistOutOptions:NSPropertyListXMLFormat_v1_0];
    
}

- (BOOL)doAppBundleIDChange:(NSString *)newBundleID {
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[_workingDir stringByAppendingPathComponent:VPAYLOAD] error:nil];
    NSString *infoPlistPath = nil;
    
    for (NSString *file in dirContents) {
        if ([[[file pathExtension] lowercaseString] isEqualToString:VAPP]) {
            infoPlistPath = [[[_workingDir stringByAppendingPathComponent:VPAYLOAD]
                              stringByAppendingPathComponent:file]
                             stringByAppendingPathComponent:VINFO_PLIST];
            break;
        }
    }
    
    return [self changeBundleIDForFile:infoPlistPath bundleIDKey:V_BUNDLE_ID_PLIST_APP newBundleID:newBundleID plistOutOptions:NSPropertyListBinaryFormat_v1_0];
}

- (BOOL)changeBundleIDForFile:(NSString *)filePath bundleIDKey:(NSString *)bundleIDKey newBundleID:(NSString *)newBundleID plistOutOptions:(NSPropertyListWriteOptions)options {
    
    NSMutableDictionary *plist = nil;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        plist = [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
        [plist setObject:newBundleID forKey:bundleIDKey];
        
        NSData *xmlData = [NSPropertyListSerialization dataWithPropertyList:plist format:options options:kCFPropertyListImmutable error:nil];
        
        return [xmlData writeToFile:filePath atomically:YES];
        
    }
    
    return NO;
}

#pragma mark - Provisioning


- (void)doProvisioning {
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[_workingDir stringByAppendingPathComponent:VPAYLOAD] error:nil];
    
    for (NSString *file in dirContents) {
        if ([[[file pathExtension] lowercaseString] isEqualToString:VAPP]) {
            self.applicationDir = [[_workingDir stringByAppendingPathComponent:VPAYLOAD] stringByAppendingPathComponent:file];
            if ([[NSFileManager defaultManager] fileExistsAtPath:[_applicationDir stringByAppendingPathComponent:@"embedded.mobileprovision"]]) {
                [[NSFileManager defaultManager] removeItemAtPath:[_applicationDir stringByAppendingPathComponent:@"embedded.mobileprovision"] error:nil];
            }
            break;
        }
    }
    
    NSString *targetPath = [_applicationDir stringByAppendingPathComponent:@"embedded.mobileprovision"];
    
    _provisionTask = [[NSTask alloc] init];
    [_provisionTask setLaunchPath:@"/bin/cp"];
    [_provisionTask setArguments:@[_pathToProvision,targetPath]];
    
    [_provisionTask launch];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkProvisioning:) userInfo:nil repeats:TRUE];
}

- (void)checkProvisioning:(NSTimer *)timer {
    if ([_provisionTask isRunning] == 0) {
        [timer invalidate];
        _provisionTask = nil;
        
        NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[_workingDir stringByAppendingPathComponent:VPAYLOAD] error:nil];
        
        for (NSString *file in dirContents) {
            if ([[[file pathExtension] lowercaseString] isEqualToString:VAPP]) {
                self.applicationDir = [[_workingDir stringByAppendingPathComponent:VPAYLOAD] stringByAppendingPathComponent:file];
                if ([[NSFileManager defaultManager] fileExistsAtPath:[_applicationDir stringByAppendingPathComponent:@"embedded.mobileprovision"]]) {
                    
                    BOOL identifierOK = NO;
                    NSString *identifierInProvisioning = @"";
                    
                    NSString *embeddedProvisioning = [NSString stringWithContentsOfFile:[_applicationDir stringByAppendingPathComponent:@"embedded.mobileprovision"] encoding:NSASCIIStringEncoding error:nil];
                    NSArray* embeddedProvisioningLines = [embeddedProvisioning componentsSeparatedByCharactersInSet:
                                                          [NSCharacterSet newlineCharacterSet]];
                    
                    for (int i = 0; i <= [embeddedProvisioningLines count]; i++) {
                        if ([[embeddedProvisioningLines objectAtIndex:i] rangeOfString:@"application-identifier"].location != NSNotFound) {
                            
                            NSInteger fromPosition = [[embeddedProvisioningLines objectAtIndex:i+1] rangeOfString:@"<string>"].location + 8;
                            
                            NSInteger toPosition = [[embeddedProvisioningLines objectAtIndex:i+1] rangeOfString:@"</string>"].location;
                            
                            NSRange range;
                            range.location = fromPosition;
                            range.length = toPosition-fromPosition;
                            
                            NSString *fullIdentifier = [[embeddedProvisioningLines objectAtIndex:i+1] substringWithRange:range];
                            
                            NSArray *identifierComponents = [fullIdentifier componentsSeparatedByString:@"."];
                            
                            if ([[identifierComponents lastObject] isEqualTo:@"*"]) {
                                identifierOK = YES;
                            }
                            
                            for (int i = 1; i < [identifierComponents count]; i++) {
                                identifierInProvisioning = [identifierInProvisioning stringByAppendingString:[identifierComponents objectAtIndex:i]];
                                if (i < [identifierComponents count]-1) {
                                    identifierInProvisioning = [identifierInProvisioning stringByAppendingString:@"."];
                                }
                            }
                            break;
                        }
                    }
                    
                    
                    NSString *infoPlist = [NSString stringWithContentsOfFile:[_applicationDir stringByAppendingPathComponent:VINFO_PLIST] encoding:NSASCIIStringEncoding error:nil];
                    if ([infoPlist rangeOfString:identifierInProvisioning].location != NSNotFound) {
                        identifierOK = YES;
                    }
                    
                    if (identifierOK) {
                        
                        //finished provisioning profile
                        if ([_delegate respondsToSelector:@selector(updateSignStatus:)]) {
                            [_delegate updateSignStatus:NSLocalizedString(@"Provisioning completed", @"Provisioning completed")];
                        }
                        
                        [self signApplication];
                    } else {
                        [self failAndClean:NSLocalizedString(@"Product identifiers don't match", @"Product identifiers don't match")];
                    }
                } else {
                    [self failAndClean:NSLocalizedString(@"Provisioning failed", @"Provisioning failed")];
                }
                break;
            }
        }
    }
}

#pragma mark - Sign

- (void)signApplication{
    
    //reset application dir
    self.applicationDir = nil;
    
    
    //check for app
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[_workingDir stringByAppendingPathComponent:VPAYLOAD] error:nil]) {
        
        //find .app folder
        if ([[[file pathExtension] lowercaseString] isEqualToString:VAPP]) {
            //found application
            self.applicationDir = [[_workingDir stringByAppendingPathComponent:VPAYLOAD] stringByAppendingPathComponent:file];
            
            //update information
            if ([_delegate respondsToSelector:@selector(updateSignStatus:)]) {
                [_delegate updateSignStatus:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Codesigning", @"Codesign"),file]];
            }
            break;
        }
    }
    
    //check if we found the application dir
    if (_applicationDir) {
        
        //load signature of the application
        NSString *resourceRulesPath = [[NSBundle mainBundle] pathForResource:@"ResourceRules" ofType:@"plist"];
        NSString *resourceRulesArgument = [NSString stringWithFormat:@"--resource-rules=%@",resourceRulesPath];
        
        //load certificate
        NSMutableArray *arguments = [NSMutableArray arrayWithObjects:@"-fs", _certificate, resourceRulesArgument, nil];
        
        //check wether need to add entitlement
        if (_pathToEntitlement) {
            [arguments addObject:[NSString stringWithFormat:@"--entitlements=%@", _pathToEntitlement]];
        }
        
        [arguments addObject:_applicationDir];
        
        self.codesignTask = [[NSTask alloc] init];
        [_codesignTask setLaunchPath:@"/usr/bin/codesign"];
        [_codesignTask setArguments:arguments];
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkSign:) userInfo:nil repeats:YES];
        
        NSPipe *pipe=[NSPipe pipe];
        [_codesignTask setStandardOutput:pipe];
        [_codesignTask setStandardError:pipe];
        NSFileHandle *handle=[pipe fileHandleForReading];
        
        [_codesignTask launch];
        
        [NSThread detachNewThreadSelector:@selector(watchCodesigning:)
                                 toTarget:self withObject:handle];
        
    }else{
        //clean up
        [self failAndClean:NSLocalizedString(@"Payload folder missing", @"Payload folder missing")];
    }
    
}

- (void)watchCodesigning:(NSFileHandle*)streamHandle {
    @autoreleasepool {
        self.codesignData = [[NSString alloc] initWithData:[streamHandle readDataToEndOfFile] encoding:NSASCIIStringEncoding];
    }
}

- (void)checkSign:(NSTimer*)timer{
    if ([_codesignTask isRunning] == 0) {
        [timer invalidate];
        _codesignTask = nil;
        
        if ([_delegate respondsToSelector:@selector(updateSignStatus:)]) {
            [_delegate updateSignStatus:NSLocalizedString(@"Codesigning completed", @"Codesigin completed")];
        }
        
        [self verifySignature];
    }else{
        [self failAndClean:NSLocalizedString(@"Failed signing application", @"Fail Signing")];
    }
}

#pragma mark - Verify

- (void)verifySignature{
    if (_applicationDir) {
        self.verifyTask = [NSTask new];
        [_verifyTask setLaunchPath:@"/usr/bin/codesign"];
        [_verifyTask setArguments:@[@"-v",_applicationDir]];
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkVerify:) userInfo:nil repeats:TRUE];
        
        if ([_delegate respondsToSelector:@selector(updateSignStatus:)]) {
            [_delegate updateSignStatus:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Verifying", @"Verifying"),[_applicationDir lastPathComponent]]];
        }
        
        NSPipe *pipe=[NSPipe pipe];
        [_verifyTask setStandardOutput:pipe];
        [_verifyTask setStandardError:pipe];
        NSFileHandle *handle=[pipe fileHandleForReading];
        
        [_verifyTask launch];
        
        [NSThread detachNewThreadSelector:@selector(watchVerify:)
                                 toTarget:self withObject:handle];
    }
}

- (void)watchVerify:(NSFileHandle*)handle{
    @autoreleasepool {
        self.verificationData = [[NSString alloc] initWithData:[handle readDataToEndOfFile] encoding:NSASCIIStringEncoding];
    }
}

- (void)checkVerify:(NSTimer*)timer{
    if ([_verifyTask isRunning] == 0) {
        [timer invalidate];
        self.verifyTask = nil;
        
        //check wether data are readable
        if ([self.verificationData length] == 0) {
            
            if ([_delegate respondsToSelector:@selector(updateSignStatus:)]) {
                [_delegate updateSignStatus:NSLocalizedString(@"Verification completed", @"Verification completed")];
            }
            
            [self zip];
        } else {
            //clean up
            [self failAndClean:[NSString stringWithFormat:@"%@\n%@",NSLocalizedString(@"Verify failed", @"Verify failed"),_verificationData]];
        }
    }
}

#pragma mark - zip

- (void)zip{
    if (_applicationDir) {
        
        //build new path for signed IPA
        NSArray *destinationPathComponents = [_pathToIPA pathComponents];
        NSString *destinationPath = @"";
        
        for (int i = 0; i < ([destinationPathComponents count]-1); i++) {
            destinationPath = [destinationPath stringByAppendingPathComponent:[destinationPathComponents objectAtIndex:i]];
        }
        
        NSString *fileName = [_pathToIPA lastPathComponent];
        fileName = [fileName substringToIndex:[fileName length]-([VIPA length]+1)];
        fileName = [fileName stringByAppendingString:VADDITIONAL_SUFFIX];
        NSString *file = [fileName stringByAppendingPathExtension:VIPA];
        
        NSString *absolutePath = [destinationPath stringByAppendingPathComponent:file];
        
        
        for (int i=1; [[NSFileManager defaultManager] fileExistsAtPath:absolutePath]; i++) {
            absolutePath = [destinationPath stringByAppendingPathComponent:[[NSString stringWithFormat:@"%@%d",fileName,i] stringByAppendingPathExtension:VIPA]];
        }
        
        destinationPath = absolutePath;
        
        self.zipTask = [[NSTask alloc] init];
        [_zipTask setLaunchPath:@"/usr/bin/zip"];
        [_zipTask setCurrentDirectoryPath:_workingDir];
        [_zipTask setArguments:@[@"-qryo",destinationPath,@"."]];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkZip:) userInfo:nil repeats:YES];
        });
        
        
        if ([_delegate respondsToSelector:@selector(updateSignStatus:)]) {
            [_delegate updateSignStatus:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Saving", @""),fileName]];
        }
        
        [_zipTask launch];
    }
}

- (void)checkZip:(NSTimer*)timer{
    if ([_zipTask isRunning] == 0) {
        [timer invalidate];
        _zipTask = nil;
        
        [self cleanTMP];
        
        //finish
        NSString *result = [[_codesignData stringByAppendingString:@"\n\n"] stringByAppendingString:_verificationData];
        
        if ([_delegate respondsToSelector:@selector(finishedSignApplication:)]) {
            [_delegate finishedSignApplication:result];
        }
    }else{
        //clean up
        //[self failAndClean:NSLocalizedString(@"Failed zip",@"Failed Zip")];
    }
}

#pragma mark - Public methods

- (void)startSign:(NSString*)ipa usingProvision:(NSString*)provision usingEntitlement:(NSString*)entitlement{
    if (_delegate) {
        
        //check wether path to ipa exist
        if (!ipa && [_delegate respondsToSelector:@selector(failSignApplication:)]) {
            [_delegate failSignApplication:NSLocalizedString(@"Path to the IPA is invalid", @"Error: Path to the IPA is invalid")];
            return;
        }
        
        //check IPA file
        if (![[NSFileManager defaultManager] fileExistsAtPath:ipa]&& [_delegate respondsToSelector:@selector(failSignApplication:)]) {
             [_delegate failSignApplication:NSLocalizedString(@"IPA file does'nt exist", @"Error: IPA file does'nt exist")];
            return;
        }
        
        //check certificate
        if (!_certificate && [_delegate respondsToSelector:@selector(failSignApplication:)]) {
            [_delegate failSignApplication:NSLocalizedString(@"Please add a certificate", @"Please add a certificate")];
            return;
        }
        
        self.pathToIPA = ipa;
        
        if ([provision length]>0) {
            self.pathToProvision = provision;
        }
        
        if ([entitlement length]>0) {
            self.pathToEntitlement = entitlement;
        }
        
        //preare
        [self prepareWorkingDirectory];
        
        //extract
        [self unzip];
        
    }
    
}

- (void)abort{
    
    //cancel unzip task
    if (_unzipTask&& [_unzipTask isRunning]) {
        [_unzipTask interrupt];
        _unzipTask = nil;
    }
    
    if(_workingDir){
        //clean tmp folder
        [self cleanTMP];
    }
}

+ (NSString*)workingDirectory{
    return WORKING_DIR;
}
@end
