/*
 
 VProvisioningManager.m
 AppSign
 
 Copyright (c) 27/09/2014 Truong Vinh Tran
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#import "VProvisioningManager.h"

#define PROVISIONING_PATH @"Library/MobileDevice/Provisioning Profiles"

@implementation VProvisioningManager

+ (VProvisioningManager*)sharedInstance{
    static dispatch_once_t predicate = 0;
    
    static VProvisioningManager *manager = nil;
    
    dispatch_once(&predicate,^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (NSDictionary*)profileToDict:(NSString*)string{
    
    NSError *error =nil;
    NSString *embeddedProvisioning = [NSString stringWithContentsOfFile:string encoding:NSASCIIStringEncoding error:&error];
    
    NSArray *components = [embeddedProvisioning componentsSeparatedByString:@"<plist version=\"1.0\">"];
    embeddedProvisioning = [components lastObject];
    components = [embeddedProvisioning componentsSeparatedByString:@"</plist>"];
    embeddedProvisioning = [components firstObject];
    embeddedProvisioning = [NSString stringWithFormat:@"<plist version=\"1.0\">%@</plist>",embeddedProvisioning];
    
    NSData *data = [embeddedProvisioning dataUsingEncoding:NSUTF8StringEncoding];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:_tmpFolder]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:_tmpFolder withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[_tmpFolder stringByAppendingPathComponent:[string lastPathComponent]]]) {
        [[NSFileManager defaultManager] removeItemAtPath:[_tmpFolder stringByAppendingPathComponent:[string lastPathComponent]] error:nil];
    }
    
    if ([data writeToFile:[_tmpFolder stringByAppendingPathComponent:[string lastPathComponent]] atomically:YES]) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithContentsOfFile:[_tmpFolder stringByAppendingPathComponent:[string lastPathComponent]]];
        [dic setObject:string forKey:@"provisioning-path"];
        [[NSFileManager defaultManager] removeItemAtPath:[_tmpFolder stringByAppendingPathComponent:[string lastPathComponent]] error:nil];
        return dic;
    }
    return @{};
}

- (NSArray*)validProfilesFor:(NSString *)codeSignID{
    
    NSString *profilesPath = [[self getHomeDirectory] stringByAppendingPathComponent:PROVISIONING_PATH];
    
    //check wether folder with profiles exist
    if ([[NSFileManager defaultManager] fileExistsAtPath:profilesPath]) {
        
        NSMutableArray *listWithProfiles = [NSMutableArray new];
        for (NSString *path in [[NSFileManager defaultManager] subpathsAtPath:profilesPath]) {
            if ([[[path pathExtension] lowercaseString] isEqualToString:@"mobileprovision"]) {
                //only profiles
                
                //convert string into dictionary
                NSDictionary *profile = [self profileToDict:[profilesPath stringByAppendingPathComponent:path]];
                
                //check wether profile match codesignID
                //entitlements information
                NSDictionary *env = [profile objectForKey:@"Entitlements"];
                
                //check expire date
                NSDate *expireOn = [profile objectForKey:@"ExpireationDate"];
                
                //skip expired profiles
                if ([expireOn timeIntervalSinceDate:[NSDate date]]>0) {
                    continue;
                }
                
                //only allow certificate with profile name
                if ([profile objectForKey:@"AppIDName"]) {
                    if ([codeSignID rangeOfString:@"iPhone Developer"].location!=NSNotFound) {
                        //developer profiles
                        
                        NSString *environment = [env objectForKey:@"aps-environment"];
                        
                        if (!environment||[environment isEqualToString:@"development"]) {
                            [listWithProfiles addObject:profile];
                        }
                    }else{
                        //distribution profiles
                        
                        NSString *environment = [env objectForKey:@"aps-environment"];
                        
                        if (!environment||[environment isEqualToString:@"production"]) {
                            [listWithProfiles addObject:profile];
                        }
                    }
                }

            }
        }
        return listWithProfiles;
    }
    
    return @[];
}

#pragma mark - Helper Methods

- (NSString*)getHomeDirectory{
    return NSHomeDirectory();
}

- (NSString*)strapKeyValue:(NSString*)key{
    NSArray *items = [key componentsSeparatedByString:@"<key>"];
    return [[[items lastObject] componentsSeparatedByString:@"</key>"] firstObject];
}

- (id)strapObjectValue:(NSString*)object forKey:(NSString*)key{
    NSArray *items = [key componentsSeparatedByString:[NSString stringWithFormat:@"<%@>",key]];
    return [[[items lastObject] componentsSeparatedByString:[NSString stringWithFormat:@"</%@>",key]] firstObject];
}

- (id)convertContentToObject:(NSString*)inputString{
    
    //string
    if ([[inputString lowercaseString] rangeOfString:@"string"].location!=NSNotFound) {
        return [self strapObjectValue:inputString forKey:@"string"];
    }
    return nil;
}


@end
