AppSign
=======

AppSign was based on iResign from Maciej Swic.
It allows to sign and resign app bundles / ipa files uusing digital certificate from Apple developer program.
This tool is aimed at enterprises users, for enterprise deployment, when the person signing the app is different than the person(s) developing it.

How to use
=======

AppSign allows you to re-sign any unencrypted ipa-file with any certificate for which you hold the corresponding private key.

1. Drag your unsigned .ipa file to the top box, or use the browse button.

2. Select certificate name from Keychain Access

3. Select available provisioning profile for signing (attention: need to select the correct profile for successful signing)

4. Click ReSign! and wait. The resigned file will be saved in the same folder as the original file.

License
=======

Copyright (c) 2014 Truong Vinh Tran

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.